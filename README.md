# Joeykessler.online

portfolio/resume site for joey kessler

## Getting Started

This website has a companion api site that is expected to be in place

### Setup api.joeykessler.online
1. Clone //gitlab.com/joeyjoejo1200/api.joeykessler.online
2. `cd` into the api.joeykessler.online directory
3. run `npm install`
4. run `node app.js`

### Setup joeykessler.online
1. Clone //gitlab.com/joeyjoe1200/joeykessler.online
2. `cd` into the joeykessler.online directory
3. run `npm install`
4. run `npm start`

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
