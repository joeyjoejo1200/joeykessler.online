import React, {useState} from "react";

export default function CardDescription(props) {
    const [data] = useState(props.data);

    return (
        <>
            {data.map((item, index) => (
                <p index={index}>{item}</p>
            ))}
        </>
    );
}